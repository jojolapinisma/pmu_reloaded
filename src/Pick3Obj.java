import java.util.Arrays;
import java.util.StringTokenizer;

public class Pick3Obj
{

    String   date;
    String[] result;
    String   time;
    String   Fireball;

    /**
     * This function will turn the String result into an Array result
     * 
     * @param result PMU String result
     * @return PMU Array result
     */
    public String[] parseToArray(String result) {
        StringTokenizer stk = new StringTokenizer(result, ",");
        String[] resArr = new String[stk.countTokens()];
        int lenght = stk.countTokens();

        for ( int i = 0; i < lenght; i++ ) {
            resArr[i] = stk.nextToken();
            //System.out.println(resArr[i]);
        }
        return resArr;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String[] getResult() {
        return result;
    }

    public void setResult(String[] result) {
        this.result = result;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFireball() {
        return Fireball;
    }

    public void setFireball(String fireball) {
        Fireball = fireball;
    }

    @Override
    public String toString() {
        return "Pick3Obj [date=" + date + ", result=" + Arrays.toString(result) + ", time=" + time + ", Fireball=" + Fireball + "]";
    }
}
