import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Cash4Life extends RawPmuData
{

    /**
     * A Collection / list of Cash4Life Result objects
     */
    ArrayList<Cash4LifeObj>     cash4LifeLineObjectList = new ArrayList<>();
    private int                 sliderSize              = 5;
    private ArrayList<String[]> resultDataList;
    /**
    * A Collection / list of PowerBall Result objects
    */
    TreeMap<Integer, String>    playTreeMaps            = new TreeMap<Integer, String>();
    private int[]               poolNumbersToPlayNextGame;
    private int[]               poolNumsToPlayLastGame;
    private boolean             checkingLastResultForWins;
    private TreeSet<Integer>    nextSliderElements;
    private String              lastGamewinningNums;
    private TreeSet<Integer>    lastSliderElements;

    /**
     * A Collection or list of Cash4Life Result objects
     */

    /**
     * This function will initialize Cash4Life property.
     * It should be call first before using any Cash4Life class object
     */
    public void initializeCash4Life(Path path, int sliderSize) {
        this.sliderSize = sliderSize;
        initializeRawData(path);
        parseC4lRawData();
        playTreeMap();
        lastGamewinningNums = new String(Arrays.toString(cash4LifeLineObjectList.get(0).getResult()));

        //setLastGamewinningNums(Arrays.toString(lastGameWinNums()));

        convertMapValuesToStringArrList();
        lastGamePoolNumbers(sliderSize);
        nextGamePoolNumbers(sliderSize);
    }

    /**
     * Method to convert TreeMap values to a List of String arrays (List<String[])
     * @param treeMap
     * @return -> ArrayList<String[]>
     */
    private ArrayList<String[]> convertMapValuesToStringArrList() {
        resultDataList = new ArrayList<>();
        if ( !playTreeMap().isEmpty() ) {
            for ( Entry<Integer, String> entry : playTreeMaps.entrySet() ) {
                resultDataList.add(new String[] { entry.getValue() });
            }
        }
        else {
            System.err.println("playTreeMaps is empty!");
        }

        return resultDataList;
    }

    /**
     * This function will generate a List (a series) of AnrrayList of PoolNumbersToPlayThisGame.
     * The number of series of ptp will depend on how quickly the series covers all the PoolNumbersToPlay numbers
     * [5, 11, 13, 14, 17, 20, 23, 26, 27, 38]
     * [1, 19, 20, 21, 26, 27, 31, 32, 34, 37]
     * [1, 2, 17, 20, 21, 26, 31, 32, 40, 41]
     * [11, 13, 21, 23, 30, 31, 32, 34, 37, 38]
     * @return an ArrayList<TreeSet<Integer>>
     */
    public ArrayList<TreeSet<Integer>> subPoolNumberToPlayList() {
        ArrayList<TreeSet<Integer>> ptp = new ArrayList<TreeSet<Integer>>();
        TreeSet<Integer> buffer = new TreeSet<Integer>();
        //System.err.println(Arrays.toString(poolNumbersToPlayNextGame));
        int myPoolNumberSize = poolNumbersToPlayNextGame.length;
        //System.out.println(myPoolNumberSize);

        int i = 0;
        while (buffer.size() != myPoolNumberSize) {
            TreeSet<Integer> myList = new TreeSet<>(randomPick(10));
            ptp.add(myList);
            System.out.println(myList);
            buffer.addAll(myList);
            i++;
        }
        System.err.println(i + " SubPoolNumber To Play the Game. Price: $" + i * 500);
        return ptp;
    }

    private TreeMap<Integer, String> playTreeMap() {

        if ( !cash4LifeLineObjectList.isEmpty() ) {
            int i = 0;
            for ( Cash4LifeObj resObj : cash4LifeLineObjectList ) {
                i++;
                playTreeMaps.put(i, Arrays.toString(resObj.getResult()));
            }
        }
        else {
            System.err.println("cash4LifeLineObjectList is empty!");
        }
        //System.out.println(ptm);
        return playTreeMaps;
    }

    /**
     * This Method will return the winning numbers that are present in the "Pool Numbers To Play"
     * 
     * This function will check if there is a winning today or not.
     * it uses today's result and checks if any of the numbers are present
     * in the "Pool Numbers To Play". its a JACK POT if all 5 are present.
     * @param slyderSize: represents the numb of results to consider for 
     * determining the "Pool Numbers To Play".
     * @return numbers from your Pool Numbers To Play that showed up in wins
     */
    public ArrayList<Integer> yourWinningNumbersForLastGame(int slyderSize) {
        checkingLastResultForWins = true;
        int Arrlegnth = cash4LifeLineObjectList.get(0).getResult().length;
        //System.err.println(lastGamePoolNumbersToPlay(slyderSize) + "Starting from day 2,Day 1 is the result that was not there the day before");
        TreeSet<Integer> arrList1 = lastGamePoolNumbers(slyderSize);
        ArrayList<Integer> arrList2 = new ArrayList<>();
        ArrayList<Integer> winningNubers = new ArrayList<>();

        for ( int k = 0; k < Arrlegnth; k++ ) {
            arrList2.add(Integer.valueOf(cash4LifeLineObjectList.get(0).getResult()[k]));
        }
        if ( arrList1.containsAll(arrList2) ) {
            //            System.err.println("YOU WIN JACK POT: " + arrList2);
            winningNubers = arrList2;
        }
        else {
            for ( int i = 0; i < arrList2.size(); i++ ) {
                if ( arrList1.contains(arrList2.get(i)) ) {
                    //                  System.err.print(arrList2.get(i) + " ");
                    winningNubers.add(arrList2.get(i));
                }
            }
        }

        /*System.out.println("My Pool to Play:");
        System.out.println(arrList1);*/

        /*        System.err.println("Winning Number is: " + Arrays.toString(Cash4LifeLineObjectList.get(0).getResult()));
        System.out.println("Winning matches are:");*/
        return winningNubers;
    }

    /**
     * Tis function will return the pool numbers used to play last game.
     * it will exclude the last game results in the process.
     * 
     * @param slyderSize
     * @return
     */
    private TreeSet<Integer> lastGamePoolNumbers(int slyderSize) {
        int i = 0;

        lastSliderElements = new TreeSet<Integer>();
        TreeSet<Integer> tempPoolNumbersToPlay = new TreeSet<Integer>();

        TreeSet<Integer> lastWinningNumber = null;
        if ( sliderSize != 0 ) {
            if ( !cash4LifeLineObjectList.isEmpty() ) {
                for ( Cash4LifeObj resObj : cash4LifeLineObjectList ) {
                    for ( int j = 0; j < resObj.getResult().length; j++ ) {
                        lastSliderElements.add(Integer.parseInt(resObj.getResult()[j]));
                    }
                    if ( i == (sliderSize) )
                        break;
                    if ( i == 0 ) {
                        lastWinningNumber = new TreeSet<Integer>(lastSliderElements);
                        lastSliderElements.clear();
                    }
                    i++;
                }
                tempPoolNumbersToPlay.addAll(populateCash4Life());
                tempPoolNumbersToPlay.removeAll(lastSliderElements);
            }
            else {
                System.err.print("cash5LineObjectList is empty. No Data! ");
            }
        }
        else {
            System.err.print("Slider size should be > 0 ->");
        }
        //System.out.println("Pool size to play:");
        setPoolNumsToPlayLastGame(convertTreeSetIntegerTo_intArr(tempPoolNumbersToPlay));
        return tempPoolNumbersToPlay;//my Pool Numbers To Play This Game

    }

    public void setPoolNumsToPlayLastGame(int[] poolNumsToPlayLastGame) {
        this.poolNumsToPlayLastGame = poolNumsToPlayLastGame;
    }

    /**
     * This function will parse each data line into a data object and populate the
     * c4lResultsList collection.
     */
    public void parseC4lRawData() {
        try {
            rawData.forEach(oneLineData -> {
                StringTokenizer stk = new StringTokenizer(oneLineData, ";: ");
                Cash4LifeObj resultObj = new Cash4LifeObj();

                if ( stk.countTokens() != 0 ) {
                    resultObj.setDate(stk.nextToken());
                    resultObj.setResult(resultObj.parseToArray(stk.nextToken()));
                    cash4LifeLineObjectList.add(resultObj);
                    stk.nextToken(); // to skip 1 token
                    stk.nextToken(); // to skip 1 token
                    resultObj.setCashBall(stk.nextToken());
                    resultObj = new Cash4LifeObj();
                }

            });
        }
        catch (Exception e) {
            System.err.println("Error in the file containt");
            System.out.println("Check data in the file");
            System.exit(0);
        }
    }

    /**
     * Will return the collection of Cash4Life result object @return; a ArrayList of
     * c5 result object
     */
    public ArrayList<Cash4LifeObj> getC4lResultsList() {
        return cash4LifeLineObjectList;
    }

    public void displayC4lParsedData() {
        System.out.print("\nCASH4LIFE PAST RESULTS\n");
        System.out.print("========================\n");
        for ( Cash4LifeObj obj : cash4LifeLineObjectList ) {
            c4lObjDisplay(obj);
        }
    }

    /**list of numbers to play 
     * This function will ignore the last x results from your next pool of numbers. you will be
     * choosing from numbers that were not in the last X game results. X is by
     * default set to 2 which means numbers found in the last two game results will
     * not be among your next choice of numbers to play, by default.nus 
     * @return -> A list of numbers to play
     */
    public TreeSet<Integer> nextGamePoolNumbers(int sliderSize) {
        int i = 0;

        nextSliderElements = new TreeSet<Integer>();
        TreeSet<Integer> myPoolNumbersToPlayThisGame = new TreeSet<Integer>();

        for ( Cash4LifeObj resObj : cash4LifeLineObjectList ) {
            if ( checkingLastResultForWins ) {
                if ( i > 0 ) {
                    for ( int j = 0; j < resObj.getResult().length; j++ ) {
                        nextSliderElements.add(Integer.parseInt(resObj.getResult()[j]));
                    }
                }
            }
            else {
                for ( int j = 0; j < resObj.getResult().length; j++ ) {
                    nextSliderElements.add(Integer.parseInt(resObj.getResult()[j]));
                }
            }
            if ( i == (sliderSize - 1) )
                break;
            i++;
        }

        myPoolNumbersToPlayThisGame.addAll(populateCash4Life());
        myPoolNumbersToPlayThisGame.removeAll(nextSliderElements);
        //System.out.println(diff);
        int[] aListInts = convertTreeSetIntegerTo_intArr(myPoolNumbersToPlayThisGame);
        setPoolNumsToPlayNextGame(aListInts);
        return myPoolNumbersToPlayThisGame;
    }

    private int[] convertTreeSetIntegerTo_intArr(TreeSet<Integer> myPoolNumbersToPlayThisGame) {
        ArrayList<Integer> aListChars = new ArrayList<>(myPoolNumbersToPlayThisGame);
        int[] aListInts = new int[aListChars.size()];

        for ( int j = 0; j < aListInts.length; j++ ) {
            aListInts[j] = aListChars.get(j).intValue();
        }
        return aListInts;
    }

    /**
     * This Method will just return the total pay out
     * 
     * This method will take the winning numbers ([12,13,16,23,32, 40], or [2,8,11,23,36, 39]) 
     * as an ArralayList and compute the total winning amount in dollars.
     * @param myWinningNumbers: checkResults(slyderSize)-> That is your win#
     * @return String value of the total winning amount in dollars.
     */
    public String yourPayOuts(ArrayList<Integer> myWinningNumbers) {
        //Need to be to be taking account of the CashBall#
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        double totalPayOut = 0;
        int payOutFor5 = 1000;
        int payOutFor4 = 500;
        int payOutFor3 = 25;
        int payOutFor2 = 4;
        int combFor5 = 6;
        int combFor4 = 15;
        int combFor3 = 20;
        int combFor2 = 15;

        if ( myWinningNumbers.size() == 2 ) {
            totalPayOut += payOutFor3 * combFor2;
        }
        else if ( myWinningNumbers.size() == 3 ) {
            totalPayOut += payOutFor3 * combFor3;
            totalPayOut += payOutFor2 * combFor2;
        }
        else if ( myWinningNumbers.size() == 4 ) {
            totalPayOut += payOutFor4 * combFor4;
            totalPayOut += payOutFor3 * combFor3;
            totalPayOut += payOutFor2 * combFor2;
        }
        else if ( myWinningNumbers.size() == 5 ) {
            totalPayOut += payOutFor4 * combFor4;
            totalPayOut += payOutFor3 * combFor3;
            totalPayOut += payOutFor2 * combFor2;
            System.err.println("Plus $1000 a Week");
        }
        else if ( myWinningNumbers.size() == 6 ) {
            //JACKPOT
            totalPayOut += payOutFor5 * combFor5;
            totalPayOut += payOutFor4 * combFor4;
            totalPayOut += payOutFor3 * combFor3;
            totalPayOut += payOutFor2 * combFor2;
            System.err.println("Plus $1000 a Day");
            //System.err.println("JackPot");
        }
        return String.valueOf(formatter.format(totalPayOut));
    }

    /**
     * This function randomly picks n numbers within a given list
     * @param resultDataList
     * @param n
     * @return
     */
    public ArrayList<Integer> randomPick(int n) {
        List<Integer> myPoolNumbersToPlay = Arrays.stream(poolNumbersToPlayNextGame).boxed().collect(Collectors.toList());
        Collections.shuffle(myPoolNumbersToPlay);
        Integer[] arr = new Integer[n];
        for ( int i = 0; i < n; i++ ) {
            //          System.out.print(obj.getRandomList(list) + ", ");
            arr[i] = myPoolNumbersToPlay.get(i);
        }
        //strArrayList is a collection of Strings as you defined.
        ArrayList<Integer> ListOfn = new ArrayList<Integer>(Arrays.asList(arr));
        System.out.println();
        //System.out.println("First Choice of 10 numbers: ---------> " +  part);
        return ListOfn;
    }

    private ArrayList<Integer> populateCash4Life() {
        int c4LMax = 60;
        ArrayList<Integer> gamePull = new ArrayList<>();
        for ( int i = 0; i < c4LMax; i++ ) {
            gamePull.add(i + (1));
        }
        //System.out.println(gamePull);
        return gamePull;
    }

    public void DownloadWebPage(String webpage) {
        try {

            // Create URL object
            URL url = new URL(webpage);
            BufferedReader readr = new BufferedReader(new InputStreamReader(url.openStream()));

            // Enter filename in which you want to download
            BufferedWriter writer = new BufferedWriter(new FileWriter("PastResult.txt"));

            // read each line from stream till end
            String line;
            rawData = new ArrayList<>();
            while ((line = readr.readLine()) != null) {
                writer.write(line);
                rawData.add(line);
                parseC4lRawData();
            }

            readr.close();
            writer.close();
            System.out.println("Successfully Downloaded.");
        }

        // Exceptions
        catch (MalformedURLException mue) {
            System.out.println("Malformed URL Exception raised");
        }
        catch (IOException ie) {
            System.out.println("IOException raised");
        }
    }

    /**
     * This function will display information about a Cash4Life result object
     * 
     * @param obj: One line result data
     */
    private void c4lObjDisplay(Cash4LifeObj obj) {
        System.out.print("Date: " + obj.getDate());
        System.out.print("\nResults: " + Arrays.toString(obj.getResult()));
        System.out.print("\nBonus Ball: " + obj.getCashBall() + "\n\n");
    }

    public void setPoolNumsToPlayNextGame(int[] poolNumbersToPlayNextGame) {
        this.poolNumbersToPlayNextGame = poolNumbersToPlayNextGame;
    }

    /**
     * Downloads a file from a specific URL and saves it to disc
     * 
     * @param fileURL HTTP URL of the file to be downloaded
     * @param saveDir path of the directory to save the file
     * @throws IOException
     */
    public static void downloadFile(String fileURL, String saveDir) throws IOException {

        File file = new File(CommonClass.getMatchingFiles(new File("DowloadedFiles/"), "Cash4Life").get(0).toString());

        if ( !file.toString().contains(currentDate()) ) {//if today's data file has not already been dowloaded, then dowload it
            final int BUFFER_SIZE = 4096;
            String gameName = "Cash4Life_";
            URL url = new URL(fileURL);
            HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            int responseCode = httpConn.getResponseCode();

            //cleanDirectory(saveDir);
            new File(CommonClass.getMatchingFiles(new File("DowloadedFiles/"), "Cash4Life").get(0).toString()).delete();

            // always check HTTP response code first
            if ( responseCode == HttpURLConnection.HTTP_OK ) {
                String fileName = gameName + currentDate() + ".txt";

                // opens input stream from the HTTP connection
                InputStream inputStream = httpConn.getInputStream();
                String saveFilePath = saveDir + File.separator + fileName;

                // opens an output stream to save into file
                FileOutputStream outputStream = new FileOutputStream(saveFilePath);

                int bytesRead = -1;
                byte[] buffer = new byte[BUFFER_SIZE];
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                outputStream.close();
                inputStream.close();

                System.out.println("File Downloaded!");
            }
            else {
                System.out.println("No file to download. Server replied HTTP code: " + responseCode);
            }
            httpConn.disconnect();
        }
    }

    /**This method will return the current date in the specified string format
     * @return 
     */
    private static String currentDate() {
        String pattern = "MM-dd-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        return date;
    }

    /** This method will clean up the chosen directory of everything inside it subfofders included
     * @param saveDir
     */
    private static void cleanDirectory(String saveDir) {
        File file = new File(saveDir);
        deleteDirectory(file);
    }

    // function to delete subdirectories and files
    private static void deleteDirectory(File file) {
        // store all the paths of files and folders present
        // inside directory
        for ( File subfile : file.listFiles() ) {

            // if it is a subfolder,e.g Rohan and Ritik,
            // recursiley call function to empty subfolder
            if ( subfile.isDirectory() ) {
                deleteDirectory(subfile);
            }

            // delete files and empty subfolders
            subfile.delete();
        }
    }

    private String[] arrayListToArr(int k, String[] strArr1) {
        for ( Integer item : yourWinningNumbersForLastGame(sliderSize) ) {
            strArr1[k] = String.valueOf(item);
            k++;
        }
        return strArr1;
    }

    /**
     * This method will count the success of the sliders of increasing size, and
     * display the success count of each slider.
     */
    public void findSlidersCount() {
        int i = 0;
        System.out.println("\nSuccess Count by Slider size:");
        while (true) {
            i++;
            int sliderCount = counSlyder(cash4LifeLineObjectList, i);
            if ( sliderCount == 0 )
                break;
        }
        System.out.println("Number of game results processed: " + cash4LifeLineObjectList.size());
    }

    /**
     * This method will go through a List of string arrays make sliders. It will
     * tell which slider size has the most occurrences in a game played. slyderSize
     * = numbers of rows of results to ignore in the next play and a success will be
     * that no number that came in the next game results also shows up in the
     * slider's numbers
     * 
     * @param cash5ResultsList: ArrayList<Cash5Obj>
     * @param slyderSize:       int
     * @return slyderCount number of time the new game result is not in the slider
     *         element
     */
    private int counSlyder(ArrayList<Cash4LifeObj> cash4LifeLineObjectList, int slyderSize) {
        int listSize = cash4LifeLineObjectList.size();
        int slyderCount = 0;
        TreeSet<String> TempSet;
        int resultsSize = cash4LifeLineObjectList.get(0).getResult().length;
        List<String[]> stringArrList = new ArrayList<>();
        /**
         * Converting all obj element results (String[] arrays) to List<String[]> before
         * processing
         */
        for ( Cash4LifeObj item : cash4LifeLineObjectList ) {
            // System.out.println(Arrays.toString(item.getResult()));
            stringArrList.add(item.getResult());
        }
        // System.out.println(stringArrList);
        /**
         * Processing all List<String[]> data to TreeSet<String> (ordered list of
         * strings)
         */
        for ( int i = listSize; i > slyderSize; i-- ) {
            TempSet = new TreeSet<String>();
            for ( int j = 0; j < slyderSize; j++ ) {
                /** Populating the TempSet with the slider string elements */
                for ( int k = 0; k < resultsSize; k++ ) {
                    TempSet.add(stringArrList.get(i - (slyderSize + 1) + j + 1)[k]);
                }
            }
            // System.out.println(TempSet);
            /** checking if new result elements are contained in slider elements list */
            for ( int k = 0; k < resultsSize; k++ ) {
                // System.out.println(stringArrList.get(i - (slyderSize + 1))[k]);
                if ( TempSet.contains(stringArrList.get(i - (slyderSize + 1))[k]) ) {
                    // System.out.println(dataAnalysis.getResultList().get(i - (slyderSize +
                    // 1)).get(k));
                    break;
                }
                else {
                }
                if ( k + 1 == resultsSize ) {
                    slyderCount++; // count only when done with the results String.format("%.Df", decimalValue);
                }
            }
        }
        System.out.println("For a slyder size of " + slyderSize + ", there is a success count of " + slyderCount + ", so "
                + String.format("%.2f", ((float) slyderCount / listSize) * 100) + "%");
        return slyderCount;
    }

    /**
     * This function will print your winning numbers in red in the last Game winning numbers
     */
    public void printMyWinInRed() {
        int k = 0;
        int size = yourWinningNumbersForLastGame(sliderSize).size();
        String[] strArr1 = new String[size];
        List<String> yourWinningArrList;
        String[] GameResArr;
        yourWinningArrList = new ArrayList<String>(Arrays.asList(arrayListToArr(k, strArr1)));
        GameResArr = lastGameWinNums();

        final int GAME_RES_SIZE = lastGameWinNums().length - 1;
        System.out.print("[");
        for ( int i = 0; i < GameResArr.length; i++ ) {
            if ( yourWinningArrList.contains(GameResArr[i]) ) {
                if ( i != GAME_RES_SIZE ) {
                    System.err.print(GameResArr[i] + ", ");
                }
                else {
                    System.err.print(GameResArr[i]);
                }
            }
            else {
                if ( i != GAME_RES_SIZE ) {
                    System.out.print(GameResArr[i] + ", ");
                }
                else {
                    System.out.print(GameResArr[i]);
                }
            }
        }
        System.out.print("]");
    }

    public int[] getPoolNumsToPlayLastGame() {
        return poolNumsToPlayLastGame;
    }

    public int[] getPoolNumsToPlayNextGame() {
        return poolNumbersToPlayNextGame;
    }

    public String getLastGamewinningNums() {
        return lastGamewinningNums;
    }

    public void setLastGamewinningNums(String lastGamewinningNums) {
        this.lastGamewinningNums = lastGamewinningNums;
    }

    public TreeSet<Integer> getLastSliderElements() {
        return lastSliderElements;

    }

    public String[] lastGameWinNums() {
        return cash4LifeLineObjectList.get(0).getResult();
    }

    public TreeSet<Integer> getNextSliderElements() {
        return nextSliderElements;
    }

}
