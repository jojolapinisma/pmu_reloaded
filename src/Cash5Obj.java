import java.util.Arrays;
import java.util.StringTokenizer;

public class Cash5Obj
{

    String   date;
    String[] result;
    String   time;

    /**
     * This function will turn the String result into an Array result
     * 
     * @param result PMU String result
     * @return PMU Array result
     */

    public String[] parseToArray(String result) {
        StringTokenizer stk = new StringTokenizer(result, ",");
        String[] resArr = new String[stk.countTokens()];
        int lenght = stk.countTokens();

        for ( int i = 0; i < lenght; i++ ) {
            resArr[i] = stk.nextToken();
            //System.out.println(resArr[i]);
        }
        return resArr;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String[] getResult() {
        return result;
    }

    public void setResult(String[] result) {
        this.result = result;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        String str = null;
        if ( time == null ) {
            str = "\nTime of Day: Once a Day.";
        }
        else {
            str = "\nTime of Day: " + time;
        }
        return "Date: " + date + "\n" + "Results: " + Arrays.toString(result) + str + "\n\n";
    }
}
