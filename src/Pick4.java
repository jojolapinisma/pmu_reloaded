import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Pick4 extends RawPmuData
{

    /**
     * A Collection / list of Pick4 Result objects
     */
    ArrayList<Pick4Obj> Pick4ResultsList = new ArrayList<>();

    /**
     * This function will parse each data line into a data object and populate the
     * Pick4ResultsList collection.
     */
    public void parsePick4RawData() {
        try {
            rawData.forEach(oneLineData -> {
                StringTokenizer stk = new StringTokenizer(oneLineData, ";: ");
                Pick4Obj resultObj = new Pick4Obj();

                if ( stk.countTokens() != 0 ) {
                    if ( stk.countTokens() == 3 ) {
                        resultObj.setDate(stk.nextToken());
                        resultObj.setTime(stk.nextToken());
                        resultObj.setResult(resultObj.parseToArray(stk.nextToken()));
                        Pick4ResultsList.add(resultObj);

                        resultObj = new Pick4Obj();
                    }
                    else if ( stk.countTokens() == 5 ) {
                        String date = stk.nextToken();
                        for ( int i = 0; i < 2; i++ ) {
                            resultObj.setDate(date);
                            resultObj.setTime(stk.nextToken());
                            resultObj.setResult(resultObj.parseToArray(stk.nextToken()));
                            Pick4ResultsList.add(resultObj);

                            resultObj = new Pick4Obj();
                        }

                        resultObj = new Pick4Obj();
                    }
                    else if ( stk.countTokens() == 9 ) {
                        String date = stk.nextToken();
                        for ( int i = 0; i < 2; i++ ) {
                            resultObj.setDate(date);
                            resultObj.setTime(stk.nextToken());
                            resultObj.setResult(resultObj.parseToArray(stk.nextToken()));
                            stk.nextToken(); // to skip 1 token
                            resultObj.setFireball(stk.nextToken());
                            Pick4ResultsList.add(resultObj);

                            resultObj = new Pick4Obj();
                        }
                    }
                }

            });
        }
        catch (Exception e) {
            System.err.println("Error in the file containt");
            System.out.println("Check data in the file");
            System.exit(0);
        }
        // System.out.println(Pick4Results);
    }

    /**
     * Will return the collection of Pick4 result object @return; a ArrayList of c5
     * result object
     */
    public ArrayList<Pick4Obj> getPick4ResultsList() {
        return Pick4ResultsList;
    }

    public void displayPick4ParsedData() {
        System.out.print("\nPICK 4 PAST RESULTS\n");
        System.out.print("===================\n");
        for ( Pick4Obj obj : Pick4ResultsList ) {
            Pick4ObjDisplay(obj);
        }

    }

    /**
     * This function will display information about a Pick4 result object
     * 
     * @param obj: One line result data
     */
    private void Pick4ObjDisplay(Pick4Obj obj) {
        System.out.print("Date: " + obj.getDate());
        System.out.print("\nResults: " + Arrays.toString(obj.getResult()));
        if ( obj.getTime() == null ) {
            System.out.print("\nTime of Day: Once a Day." + "\n\n");
        }
        else {
            System.out.print("\nTime of Day: " + obj.getTime() + "\n");
        }
        System.out.print("Fireball: " + obj.getFireball() + "\n\n");
    }

}
