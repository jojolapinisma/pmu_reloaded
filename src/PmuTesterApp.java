import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class PmuTesterApp {
    static Path RawPmuDataPath;

    public static void main(String[] args) {

         cash5();
        //bankAMillions();
        // cash4Life();
        // megaMillions();
        // powerBall();

        // pick4();
        // pick3();

    }

    private static void pick3() {
        System.out.println("\npick3");
        RawPmuDataPath = Paths.get("res/Pick3_7_13_2021.txt");
        Pick3 p3 = new Pick3();

        p3.readRawPmuDataFile(RawPmuDataPath);
        // p3.displayRawData(RawPmuDataPath);

        p3.parsePick3RawData();
        p3.getPick3ResultsList();
        p3.displayPick3ParsedData();

        for (Pick3Obj resObj : p3.getPick3ResultsList()) {
            System.out.println(Arrays.toString(resObj.getResult()));
        }
    }

    private static void pick4() {
        System.out.println("\npick3");
        RawPmuDataPath = Paths.get("res/Pick4_7_13_2021.txt");
        Pick4 cp4 = new Pick4();

        cp4.readRawPmuDataFile(RawPmuDataPath);
        // cp4.displayRawData(RawPmuDataPath);

        cp4.parsePick4RawData();
        cp4.getPick4ResultsList();
        cp4.displayPick4ParsedData();

        for (Pick4Obj resObj : cp4.getPick4ResultsList()) {
            System.out.println(Arrays.toString(resObj.getResult()));
        }
    }

    private static void megaMillions() {
        int sliderSize = 5;
        CommonClass.GameName GAME_NAME = CommonClass.GameName.MEGAMILLIONS;
        System.out.println("\nMegaMillions" + ", sliderSize:" + sliderSize);

        RawPmuDataPath = Paths.get(CommonClass.manedgeDataFiles(CommonClass.GameName.MEGAMILLIONS));

        MegaMillions mm = new MegaMillions();
        mm.initializeMegaMillions(RawPmuDataPath, sliderSize);

        /*
         * System.out.println("Slider Elements for Next Game: " +
         * mm.getNextSliderElements() + " for Next Game");
         * System.out.println("My Winning Numbers: " +
         * mm.yourWinningNumbersForLastGame(sliderSize));
         * System.err.println("Pool for Last Play: " +
         * Arrays.toString(mm.getPoolNumsToPlayLastGame()));
         * System.err.println("Pool for Next Play: " +
         * Arrays.toString(mm.getPoolNumsToPlayNextGame()));
         * System.err.println("My Gains: " +
         * mm.yourPayOuts(mm.yourWinningNumbersForLastGame(sliderSize)));
         */

        // c5.findSlidersCount();
        System.out.println("\n" + GAME_NAME + "  LastGame Results: ");
        System.out.println("---------------------------");
        System.out.println("Last Game sliderSize: 5");
        System.out.println("Last Game Slider Elements: " + mm.getLastSliderElements());
        System.out.println("Last Game Pool to Play: " + Arrays.toString(mm.getPoolNumsToPlayLastGame()));
        System.out.println("Last Game Winning Numbers: " + mm.getLastGamewinningNums());
        System.out.print("Print My Wins In Red: ");
        mm.printMyWinInRed();
        System.out.println("\nMy Last Game Winning Numbers: " + mm.yourWinningNumbersForLastGame(sliderSize));
        System.out.println("My Gains:" + mm.yourPayOuts(mm.yourWinningNumbersForLastGame(sliderSize)));
        System.out.println("");
        System.out.println(GAME_NAME + " Next Game Proposal: ");
        System.out.println("-----------------------------");
        System.out.println("Next Game sliderSize: 5");
        System.out.println("Next Game Slider Elements: " + mm.getNextSliderElements());
        System.out.println("Next Game Single Pool to Play: " + Arrays.toString(mm.getPoolNumsToPlayNextGame()));
        System.out.println("Next Game 10 by 10 Pool to Play: ");
        mm.subPoolNumberToPlayList();

        LotteryResultsParser.make_DeepLearningData(RawPmuDataPath, GAME_NAME, sliderSize);
    }

    private static void powerBall() {
        int sliderSize = 5;
        CommonClass.GameName GAME_NAME = CommonClass.GameName.POWERBALL;
        System.out.println("\nPowerBall" + ", sliderSize:" + sliderSize);

        PowerBall pb = new PowerBall();

        RawPmuDataPath = Paths.get(CommonClass.manedgeDataFiles(CommonClass.GameName.POWERBALL));
        pb.initializePowerBall(RawPmuDataPath, sliderSize);

        /*
         * System.out.println("Slider Elements for Next Game: " + pb.getSliderElements()
         * + " for Next Game"); System.out.println("My Winning Numbers: " +
         * pb.yourWinningNumbersForLastGame(sliderSize));
         * System.err.println("Pool for Last Play: " +
         * Arrays.toString(pb.getPoolNumsToPlayLastGame()));
         * System.err.println("Pool for Next Play: " +
         * Arrays.toString(pb.getPoolNumsToPlayNextGame()));
         * System.err.println("My Gains: " +
         * pb.yourPayOuts(pb.yourWinningNumbersForLastGame(sliderSize)));
         */

        // c5.findSlidersCount();
        System.out.println("\n" + GAME_NAME + "  LastGame Results: ");
        System.out.println("---------------------------");
        System.out.println("Last Game sliderSize: 5");
        System.out.println("Last Game Slider Elements: " + pb.getLastSliderElements());
        System.out.println("Last Game Pool to Play: " + Arrays.toString(pb.getPoolNumsToPlayLastGame()));
        System.out.println("Last Game Winning Numbers: " + pb.getLastGamewinningNums());
        System.out.print("Print My Wins In Red: ");
        pb.printMyWinInRed();
        System.out.println("\nMy Last Game Winning Numbers: " + pb.yourWinningNumbersForLastGame(sliderSize));
        System.out.println("My Gains:" + pb.yourPayOuts(pb.yourWinningNumbersForLastGame(sliderSize)));
        System.out.println("");
        System.out.println(GAME_NAME + " Next Game Proposal: ");
        System.out.println("-----------------------------");
        System.out.println("Next Game sliderSize: 5");
        System.out.println("Next Game Slider Elements: " + pb.getNextSliderElements());
        System.out.println("Next Game Single Pool to Play: " + Arrays.toString(pb.getPoolNumsToPlayNextGame()));
        System.out.println("Next Game 10 by 10 Pool to Play: ");
        pb.subPoolNumberToPlayList();

        LotteryResultsParser.make_DeepLearningData(RawPmuDataPath, GAME_NAME, sliderSize);
        // pb.createDeepLearningData(sliderSize);
        // c5.winLossTestStats(c5.getMap_Key_PoolNumbToPlay_PairsList(), 27, 4);

        // c5.genrateSubPoolNumberToPlayList();
        // c5.counSlider(sliderSize);
    }

    private static void cash4Life() {
        int sliderSize = 5;
        CommonClass.GameName GAME_NAME = CommonClass.GameName.CASH_4_LIFE;
        Cash4Life c4l = new Cash4Life();

        RawPmuDataPath = Paths.get(CommonClass.manedgeDataFiles(CommonClass.GameName.CASH_4_LIFE));
        c4l.initializeCash4Life(RawPmuDataPath, sliderSize);

        /*
         * System.out.println("Slider Elements for Next Game: " +
         * c4l.getNextSliderElements() + " for Next Game");
         * System.out.println("My Winning Numbers: " +
         * c4l.yourWinningNumbersForLastGame(sliderSize));
         * System.err.println("Pool for Last Play: " +
         * Arrays.toString(c4l.getPoolNumsToPlayLastGame()));
         * System.err.println("Pool for Next Play: " +
         * Arrays.toString(c4l.getPoolNumsToPlayNextGame()));
         * System.err.println("My Gains: " +
         * c4l.yourPayOuts(c4l.yourWinningNumbersForLastGame(sliderSize)));
         * System.err.println("::::::::");
         */

        // System.out.println("\nCash4Life" + ", sliderSize:" + sliderSize);
        c4l.findSlidersCount();
        System.out.println("\n" + GAME_NAME + "  LastGame Results: ");
        System.out.println("---------------------------");
        System.out.println("Last Game sliderSize: 5");
        System.out.println("Last Game Slider Elements: " + c4l.getLastSliderElements());
        System.out.println("Last Game Pool to Play: " + Arrays.toString(c4l.getPoolNumsToPlayLastGame()));
        System.out.println("Last Game Winning Numbers: " + c4l.getLastGamewinningNums());
        System.out.print("Print My Wins In Red: ");
        c4l.printMyWinInRed();
        System.out.println("\nMy Last Game Winning Numbers: " + c4l.yourWinningNumbersForLastGame(sliderSize));
        System.out.println("My Gains:" + c4l.yourPayOuts(c4l.yourWinningNumbersForLastGame(sliderSize)));
        System.out.println("");
        System.out.println(GAME_NAME + " Next Game Proposal: ");
        System.out.println("-----------------------------");
        System.out.println("Next Game sliderSize: 5");
        System.out.println("Next Game Slider Elements: " + c4l.getNextSliderElements());
        System.out.println("Next Game Single Pool to Play: " + Arrays.toString(c4l.getPoolNumsToPlayNextGame()));
        System.out.println("Next Game 10 by 10 Pool to Play: ");
        c4l.subPoolNumberToPlayList();

        LotteryResultsParser.make_DeepLearningData(RawPmuDataPath, GAME_NAME, sliderSize);

    }

    private static void bankAMillions() {
        int sliderSize = 3;
        CommonClass.GameName GAME_NAME = CommonClass.GameName.BANK_A_MILLION;
        System.out.println("\nBankAMillions" + ", sliderSize:" + sliderSize);
        BankaMillion bam = new BankaMillion();

        RawPmuDataPath = Paths.get(CommonClass.manedgeDataFiles(CommonClass.GameName.BANK_A_MILLION));
        bam.initializeCash5(RawPmuDataPath, sliderSize);

        /*
         * System.out.println("Slider Elements for Next Game: " +
         * bam.getSliderElements() + " for Next Game");
         * System.out.println("My Winning Numbers: " +
         * bam.yourWinningNumbersForLastGame(sliderSize));
         * System.err.println("Pool for Last Play: " +
         * Arrays.toString(bam.getPoolNumsToPlayLastGame()));
         * System.err.println("Pool for Next Play: " +
         * Arrays.toString(bam.getPoolNumsToPlayNextGame()));
         * System.err.println("My Gains: " +
         * bam.yourPayOuts(bam.yourWinningNumbersForLastGame(sliderSize)));
         */

        // c5.findSlidersCount();
        System.out.println("\n" + GAME_NAME + "  LastGame Results: ");
        System.out.println("---------------------------");
        System.out.println("Last Game sliderSize: 5");
        System.out.println("Last Game Slider Elements: " + bam.getLastSliderElements());
        System.out.println("Last Game Pool to Play: " + Arrays.toString(bam.getPoolNumsToPlayLastGame()));
        System.out.println("Last Game Winning Numbers: " + bam.getLastGamewinningNums());
        System.out.print("Print My Wins In Red: ");
        bam.printMyWinInRed();
        System.out.println("\nMy Last Game Winning Numbers: " + bam.yourWinningNumbersForLastGame(sliderSize));
        System.out.println("My Gains:" + bam.yourPayOuts(bam.yourWinningNumbersForLastGame(sliderSize)));
        System.out.println("");
        System.out.println(GAME_NAME + " Next Game Proposal: ");
        System.out.println("-----------------------------");
        System.out.println("Next Game sliderSize: 5");
        System.out.println("Next Game Slider Elements: " + bam.getNextSliderElements());
        System.out.println("Next Game Single Pool to Play: " + Arrays.toString(bam.getPoolNumsToPlayNextGame()));
        System.out.println("Next Game 10 by 10 Pool to Play: ");
        bam.subPoolNumberToPlayList();

        LotteryResultsParser.make_DeepLearningData(RawPmuDataPath, GAME_NAME, sliderSize);

    }

    private static void cash5() {
        int sliderSize = 3;
        CommonClass.GameName GAME_NAME = CommonClass.GameName.CASH5;
        System.out.println("\nCash5" + ", sliderSize:" + sliderSize);
        Cash5 c5 = new Cash5();

        RawPmuDataPath = Paths.get(CommonClass.manedgeDataFiles(CommonClass.GameName.CASH5));
        c5.initializeCash5(RawPmuDataPath, sliderSize);

        // c5.findSlidersCount();
        System.out.println("\n" + GAME_NAME + "  LastGame Results: ");
        System.out.println("---------------------------");
        System.out.println("Last Game sliderSize: " + sliderSize);
        System.out.println("Last Game Slider Elements: " + c5.getLastSliderElements());
        System.out.println("Last Game Pool to Play: " + Arrays.toString(c5.getPoolNumsToPlayLastGame()));
        System.out.println("Last Game Winning Numbers: " + c5.getLastGamewinningNums());
        System.out.print("Print My Wins In Red: ");
        c5.printMyWinInRed();
        System.out.println("\nMy Last Game Winning Numbers: " + c5.yourWinningNumbersForLastGame(sliderSize));
        System.out.println("My Gains:" + c5.yourPayOuts(c5.yourWinningNumbersForLastGame(sliderSize)));
        System.out.println("");
        System.out.println(GAME_NAME + " Next Game Proposal: ");
        System.out.println("-----------------------------");
        System.out.println("Next Game sliderSize: " + sliderSize);
        System.out.println("Next Game Slider Elements: " + c5.getNextSliderElements());
        System.out.println("Next Game Single Pool to Play: " + Arrays.toString(c5.getPoolNumsToPlayNextGame()) + "\n");

        System.out.println("In Red is the \"Next Game Pool Numbers To Play\": ");
        
        LotteryResultsParser.make_DeepLearningData(RawPmuDataPath, GAME_NAME, sliderSize);
        
        //c5.genrateSubPoolNumberToPlayList(10);
       // c5.createDeepLearningData(sliderSize);
        //c5.winLossTestStats(c5.getMap_Key_PoolNumbToPlay_PairsList(), 27, 4);

        // c5.genrateSubPoolNumberToPlayList();
        c5.counSlider(sliderSize);

    }

}
