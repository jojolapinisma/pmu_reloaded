import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Path;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * @author jilboudo
 *
 */
public class Cash5 extends RawPmuData
{

    private int                                         sliderSize;

    /**
     * A Collection or list of cash5 Result objects
     */
    ArrayList<Cash5Obj>                                 cash5LineObjectList = new ArrayList<>();

    /**
     * A Collection or TreeMap (key and ordered values map) of cash5 Result
     */
    TreeMap<Integer, String>                            playTreeMaps        = new TreeMap<Integer, String>();
    /**
     * This holds the pool #s to build my combination out of
     */
    /**
     * All past result list in arrayList of String[]
     */
    private ArrayList<String[]>                         resultDataList;

    private int[]                                       poolNumbersToPlayNextGame;
    private int[]                                       poolNumsToPlayLastGame;

    private TreeSet<Integer>                            nextSliderElements;

    private TreeSet<Integer>                            lastSliderElements;

    private String                                      lastGamewinningNums;

    private Map<String[], ArrayList<String>>            map_Key_PoolNumbToPlay_PairsList;

    private ArrayList<Map<String[], ArrayList<String>>> allGameKey_PoolNumb_MapList;

    /**
     * This function will initialize Cash5 property. It should be call first before
     * using any Cash5 class object
     */
    public void initializeCash5(Path path, int sliderSize) {
        this.sliderSize = sliderSize;
        initializeRawData(path);
        parseCash5RawData();
        playTreeMap();
        lastGamewinningNums = new String(Arrays.toString(cash5LineObjectList.get(0).getResult()));
        convertMapValuesToStringArrList();
        lastGamePoolNumbers(sliderSize);
        nextGamePoolNumbers(sliderSize);
    }

    /**
     * This function will parse each data line into a data object and populate the
     * cash5ResultsList collection.
     */
    private void parseCash5RawData() {

        if ( !rawData.isEmpty() ) {
            try {
                rawData.forEach(oneLineData -> {
                    StringTokenizer stk = new StringTokenizer(oneLineData, ";: ");
                    Cash5Obj resultObj = new Cash5Obj();

                    if ( stk.countTokens() != 0 ) {
                        if ( stk.countTokens() == 2 ) {
                            resultObj.setDate(stk.nextToken());
                            resultObj.setResult(resultObj.parseToArray(stk.nextToken()));
                            cash5LineObjectList.add(resultObj);

                            resultObj = new Cash5Obj();
                        }
                        else if ( stk.countTokens() == 3 ) {
                            resultObj.setDate(stk.nextToken());
                            resultObj.setTime(stk.nextToken());
                            resultObj.setResult(resultObj.parseToArray(stk.nextToken()));
                            cash5LineObjectList.add(resultObj);

                            resultObj = new Cash5Obj();
                        }
                        else if ( stk.countTokens() == 5 ) {
                            String date = stk.nextToken();
                            for ( int i = 0; i < 2; i++ ) {
                                resultObj.setDate(date);
                                resultObj.setTime(stk.nextToken());
                                resultObj.setResult(resultObj.parseToArray(stk.nextToken()));
                                cash5LineObjectList.add(resultObj);

                                resultObj = new Cash5Obj();
                            }
                        }
                    }

                });
            }
            catch (Exception e) {
                System.err.println("Error in the file containt");
                System.out.println("Check data in the file");
                System.exit(0);
            }
        }
        else {
            System.out.println("rawData is empty!");
        }
        // System.out.println(cash5Results);
    }

    /**
     * This function will generate a List (a serie) of ArrayList of
     * PoolNumbersToPlayNextGame. The number of series of lists will depend on how
     * quickly the series covers all the PoolNumbersToPlay numbers Exemple: [5, 11,
     * 13, 14, 17, 20, 23, 26, 27, 38] [1, 19, 20, 21, 26, 27, 31, 32, 34, 37] [1,
     * 2, 17, 20, 21, 26, 31, 32, 40, 41] [11, 13, 21, 23, 30, 31, 32, 34, 37, 38]
     * 
     * @return an ArrayList<TreeSet<Integer>>
     */
    public ArrayList<TreeSet<Integer>> genrateSubPoolNumberToPlayList(int subPoolNubElements) {
        ArrayList<TreeSet<Integer>> ptp = new ArrayList<TreeSet<Integer>>();
        TreeSet<Integer> buffer = new TreeSet<Integer>();
        System.err.println(Arrays.toString(poolNumbersToPlayNextGame));
        int myPoolNumberSize = poolNumbersToPlayNextGame.length;
        System.out.println("Pool Size = " + myPoolNumberSize);

        System.out.println("Series of " + subPoolNubElements + " of the above Pool Numbers To Play: $252 for each.");
        int i = 0;
        while (buffer.size() != myPoolNumberSize) {
            TreeSet<Integer> myList = new TreeSet<>(randomPick(subPoolNubElements));
            ptp.add(myList);
            System.out.println(myList);
            buffer.addAll(myList);
            i++;
        }
        System.err.println(i + " Series of " + subPoolNubElements + " Elements");
        return ptp;
    }

    /**
     * l
     * 
     * 
     * This function will ignore the last x (sliderSize) results from your next pool
     * of numbers to play. You will be choosing your numbers from numbers that were
     * not in the last X (sliderSize) game results. X (sliderSize) is by default set
     * to 2 which means numbers found in the last two game results will not be among
     * your next choice of numbers to play, by default.
     * 
     * @return -> A list of numbers to play that does not contain the last X
     *         (sliderSize) of numbers
     */
    private TreeSet<Integer> nextGamePoolNumbers(int sliderSize) {
        int i = 0;

        nextSliderElements = new TreeSet<Integer>();
        TreeSet<Integer> myPoolNumbersToPlayThisGame = new TreeSet<Integer>();

        if ( sliderSize != 0 ) {
            if ( !cash5LineObjectList.isEmpty() ) {
                for ( Cash5Obj resObj : cash5LineObjectList ) {
                    for ( int j = 0; j < resObj.getResult().length; j++ ) {
                        nextSliderElements.add(Integer.parseInt(resObj.getResult()[j]));
                    }
                    if ( i == (sliderSize - 1) )
                        break;
                    i++;
                }
                myPoolNumbersToPlayThisGame.addAll(populateCash41());
                myPoolNumbersToPlayThisGame.removeAll(nextSliderElements);
            }
            else {
                System.err.print("cash5LineObjectList is empty. No Data! ");
            }
        }
        else {
            System.err.print("Slider size should be > 0 ->");
        }
        // System.out.println("Pool size to play:");
        int[] aListInts = convertTreeSetIntegerTo_intArr(myPoolNumbersToPlayThisGame);
        setPoolNumsToPlayNextGame(aListInts);

        return myPoolNumbersToPlayThisGame;// my Pool Numbers To Play This Game
    }

    /**
     * This function Can actually be a common utility function
     * @param myPoolNumbersToPlayThisGame
     * @return
     */
    private int[] convertTreeSetIntegerTo_intArr(TreeSet<Integer> myPoolNumbersToPlayThisGame) {
        ArrayList<Integer> aListChars = new ArrayList<>(myPoolNumbersToPlayThisGame);
        int[] aListInts = new int[aListChars.size()];

        for ( int j = 0; j < aListInts.length; j++ ) {
            aListInts[j] = aListChars.get(j).intValue();
        }
        return aListInts;
    }

    /**
     * This Method will return the winning numbers that are present in the "Pool
     * Numbers To Play"
     * 
     * This function will check if there is a winning today or not. it uses today's
     * result and checks if any of the numbers are present in the "Pool Numbers To
     * Play". its a JACK POT if all 5 are present.
     * 
     * @param sliderSize: represents the numb of results to consider for determining
     *                    the "Pool Numbers To Play".
     * @return numbers from your Pool Numbers To Play that showed up in wins
     */
    public ArrayList<Integer> yourWinningNumbersForLastGame(int sliderSize) {
        int Arrlegnth = cash5LineObjectList.get(0).getResult().length;
        TreeSet<Integer> arrList1 = lastGamePoolNumbers(sliderSize);
        ArrayList<Integer> arrList2 = new ArrayList<>();
        ArrayList<Integer> winningNubers = new ArrayList<>();

        for ( int k = 0; k < Arrlegnth; k++ ) {
            arrList2.add(Integer.valueOf(cash5LineObjectList.get(0).getResult()[k]));
        }
        if ( arrList1.containsAll(arrList2) ) {
            // System.err.println("YOU WIN JACK POT: " + arrList2);
            winningNubers = arrList2;
        }
        else {
            for ( int i = 0; i < arrList2.size(); i++ ) {
                if ( arrList1.contains(arrList2.get(i)) ) {
                    // System.err.print(arrList2.get(i) + " ");
                    winningNubers.add(arrList2.get(i));
                }
            }
        }

        return winningNubers;
    }

    /**
     * Tis function will return the last pool numbers used to play last game. it will
     * exclude the last game results in the process.
     * 
     * @param sliderSize
     * @return
     */
    private TreeSet<Integer> lastGamePoolNumbers(int sliderSize) {
        int i = 0;

        lastSliderElements = new TreeSet<Integer>();
        TreeSet<Integer> tempPoolNumbersToPlay = new TreeSet<Integer>();

        TreeSet<Integer> lastWinningNumber = null;
        if ( sliderSize != 0 ) {
            if ( !cash5LineObjectList.isEmpty() ) {
                for ( Cash5Obj resObj : cash5LineObjectList ) {
                    for ( int j = 0; j < resObj.getResult().length; j++ ) {
                        lastSliderElements.add(Integer.parseInt(resObj.getResult()[j]));
                    }
                    if ( i == (sliderSize) )
                        break;
                    if ( i == 0 ) {
                        lastWinningNumber = new TreeSet<Integer>(lastSliderElements);
                        lastSliderElements.clear();
                    }
                    i++;
                }
                tempPoolNumbersToPlay.addAll(populateCash41());
                tempPoolNumbersToPlay.removeAll(lastSliderElements);
            }
            else {
                System.err.print("cash5LineObjectList is empty. No Data! ");
            }
        }
        else {
            System.err.print("Slider size should be > 0 ->");
        }
        // System.out.println("Pool size to play:");
        setPoolNumsToPlayLastGame(convertTreeSetIntegerTo_intArr(tempPoolNumbersToPlay));
        return tempPoolNumbersToPlay;// my Pool Numbers To Play This Game
    }

    /**
     * This Method will just return the total pay out
     * 
     * This method will take the winning numbers ([12,13,16,23,32], or
     * [2,8,11,23,36]) as an ArralayList and compute the total winning amount in
     * dollars.
     * 
     * @param myWinningNumbers: checkResults(sliderSize)-> That is your win#
     * @return String value of the total winning amount in dollars.
     */
    public String yourPayOuts(ArrayList<Integer> myWinningNumbers) {
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        int payOutFor4 = 200;
        int payOutFor3 = 5;
        int payOutFor2 = 1;
        int combFor4 = 5;
        int combFor2_3 = 10;

        double totalPayOut = 0;
        if ( myWinningNumbers.size() == 2 ) {
            totalPayOut += payOutFor2 * combFor2_3;
        }
        else if ( myWinningNumbers.size() == 3 ) {
            totalPayOut += payOutFor2 * combFor2_3;
            totalPayOut += payOutFor3 * combFor2_3;
        }
        else if ( myWinningNumbers.size() == 4 ) {
            totalPayOut += payOutFor2 * combFor2_3;
            totalPayOut += payOutFor3 * combFor2_3;
            totalPayOut += payOutFor4 * combFor4;
        }
        else if ( myWinningNumbers.size() == 5 ) {
            // JACKPOT
            totalPayOut += payOutFor2 * combFor2_3;
            totalPayOut += payOutFor3 * combFor2_3;
            totalPayOut += payOutFor4 * combFor4;
            totalPayOut += 100000;
            // System.err.println("JackPot");
        }
        return String.valueOf(formatter.format(totalPayOut));
    }

    /**
     * This method will filter out (removeAll) all combinations that has already
     * showed up as winning numbers in the past It will return a new list of
     * combinations that has never came as winners to be played.
     * 
     * @return List<String[]>
     */
    public List<String[]> filterCombinations() {
        // you will need 2 lists
        // 1) List of old results
        // 2) List of your PoolNumbersToPlay

        // Then you will use removeAll() from the collections framework to filter out
        // unwanted combination numbers and return the List<String[]> to be played.

        return resultDataList;
    }

    /**
     * This function take the myPoolNumbersToPlayThisGame as an array and return all
     * the combinations for a game of 5
     * 
     * @param arr           myPoolNumbersToPlayThisGame
     * @param arrLength     length of myPoolNumbersToPlayThisGame
     * @param gameResLength length of 1 result: 5 for cash5
     * @return all possible combinations
     */
    public ArrayList<String[]> printCombination_Cash5(int arr[], int arrLength, int gameResLength) {

        int data[] = new int[gameResLength];
        return combinationUtil_Cash5(arr, data, 0, arrLength - 1, 0, gameResLength);
    }

    private static ArrayList<String[]> combinationUtil_Cash5(int arr[], int data[], int start, int end, int index, int r) {
        ArrayList<String[]> arl = new ArrayList<>();
        if ( index == r ) {
            String[] tmp = new String[r];
            for ( int j = 0; j < r; j++ ) {
                tmp[j] = Integer.toString(data[j]);
            }
            arl.add(tmp);
            System.out.println(Arrays.toString(tmp)); // send data for display
            return arl;
        }
        for ( int i = start; i <= end && end - i + 1 >= r - index; i++ ) {
            data[index] = arr[i];
            combinationUtil_Cash5(arr, data, i + 1, end, index + 1, r);
        }
        return arl;
    }

    /**
     * Can actually be a common function
     * Method to convert TreeMap to ArrayList of keys and ArrayList of values
     * 
     * @param treeMap
     * @return -> ArrayList of values
     */
    private List<List<String>> convertMapToList(TreeMap<Integer, String> treeMap) {
        // Extract the keys from the TreeMap
        // using TreeMap.keySet() and
        // assign them to keyList of type ArrayList
        ArrayList<Integer> keyList = new ArrayList<Integer>(treeMap.keySet());

        // Extract the values from the TreeMap
        // using TreeMap.values() and
        // assign them to valueList of type ArrayList
        ArrayList<String> valueList = new ArrayList<String>(treeMap.values());
        List<List<String>> resultList = new ArrayList<>();

        for ( Entry<Integer, String> entry : playTreeMaps.entrySet() ) {
            ArrayList<String> tempList = new ArrayList<>();

            tempList.add((entry.getValue()));
            resultList.add(tempList);
        }

        // printing the keyList
        // System.out.println("List of keys of the given Map : " + keyList);

        // printing the valueList
        // System.out.println("List of values of the given Map : " + valueList);
        return resultList;
    }

    /**
     * This function Can actually be a common utility function
     * Method to convert TreeMap values to a List of String arrays (List<String[])
     * 
     * @param treeMap
     * @return -> ArrayList<String[]>
     */
    private ArrayList<String[]> convertMapValuesToStringArrList() {
        resultDataList = new ArrayList<>();
        if ( !playTreeMap().isEmpty() ) {
            for ( Entry<Integer, String> entry : playTreeMaps.entrySet() ) {
                resultDataList.add(new String[] { entry.getValue() });
            }
        }
        else {
            System.err.println("playTreeMaps is empty!");
        }
        return resultDataList;
    }

    private TreeMap<Integer, String> playTreeMap() {

        if ( !cash5LineObjectList.isEmpty() ) {
            int i = 0;
            for ( Cash5Obj resObj : cash5LineObjectList ) {
                i++;
                playTreeMaps.put(i, Arrays.toString(resObj.getResult()));
            }
        }
        else {
            System.err.println("cash5ResultsList is empty!");
        }
        // System.out.println(ptm);
        return playTreeMaps;
    }

    /**
     * This method will count the success of the sliders of increasing size, and
     * display the success count of each slider.
     */
    public void findSlidersCount() {
        int i = 0;
        System.out.println("\nSuccess Count by Slider size:");
        while (true) {
            i++;
            int sliderCount = counSlider(i);
            if ( sliderCount == 0 )
                break;
        }
        System.out.println("Number of game results processed: " + cash5LineObjectList.size());
    }

    /**
     * This method will go through a List of string arrays make sliders. It will
     * tell which slider size has the most occurrences in a game played. sliderSize
     * = numbers of rows of results to ignore in the next play and a success will be
     * that no number that came in the next game results also shows up in the
     * slider's numbers
     * 
     * @param cash5ResultsList: ArrayList<Cash5Obj>
     * @param sliderSize:       int
     * @return sliderCount number of time the new game result is not in the slider
     *         element
     */
    public int counSlider(int variableSliderSize) {
        int listSize = cash5LineObjectList.size();
        int sliderCount = 0;
        TreeSet<String> TempSet;
        int resultsSize = cash5LineObjectList.get(0).getResult().length;
        List<String[]> stringArrList = new ArrayList<>();
        /**
         * Converting all obj element results (String[] arrays) to List<String[]> before
         * processing
         */
        for ( Cash5Obj item : cash5LineObjectList ) {
            // System.out.println(Arrays.toString(item.getResult()));
            stringArrList.add(item.getResult());
        }
        // System.out.println(stringArrList);
        /**
         * Processing all List<String[]> data to TreeSet<String> (ordered list of
         * strings)
         */
        for ( int i = listSize; i > variableSliderSize; i-- ) {
            TempSet = new TreeSet<String>();
            for ( int j = 0; j < variableSliderSize; j++ ) {
                /** Populating the TempSet with the slider string elements */
                for ( int k = 0; k < resultsSize; k++ ) {
                    TempSet.add(stringArrList.get(i - (variableSliderSize + 1) + j + 1)[k]);
                }
            }
            // System.out.println(TempSet);
            /** checking if new result elements are contained in slider elements list */
            for ( int k = 0; k < resultsSize; k++ ) {
                // System.out.println(stringArrList.get(i - (sliderSize + 1))[k]);
                if ( TempSet.contains(stringArrList.get(i - (variableSliderSize + 1))[k]) ) {
                    // System.out.println(dataAnalysis.getResultList().get(i - (sliderSize +
                    // 1)).get(k));
                    break;
                }
                else {
                }
                if ( k + 1 == resultsSize ) {
                    sliderCount++; // count only when done with the results String.format("%.Df", decimalValue);
                }
            }
        }
        System.out.println("For a slider size of " + variableSliderSize + ", there is a success count of " + sliderCount + ", so "
                + String.format("%.2f", ((float) sliderCount / listSize) * 100) + "%");
        return sliderCount;
    }

    private ArrayList<Integer> populateCash41() {
        int c5Max = 41;
        ArrayList<Integer> gamePull = new ArrayList<>();
        for ( int i = 0; i < c5Max; i++ ) {
            gamePull.add(i + (1));
        }
        // System.out.println(gamePull);
        return gamePull;
    }

    /**
     * This function randomly picks n numbers within a given list
     * (myPoolNumbersToPlayThisGame)
     * 
     * @param resultDataList
     * @param n
     * @return
     */
    public ArrayList<Integer> randomPick(int n) {
        List<Integer> myPoolNumbersToPlay = Arrays.stream(poolNumbersToPlayNextGame).boxed().collect(Collectors.toList());
        Collections.shuffle(myPoolNumbersToPlay);
        Integer[] arr = new Integer[n];
        for ( int i = 0; i < n; i++ ) {
            // System.out.print(obj.getRandomList(list) + ", ");
            arr[i] = myPoolNumbersToPlay.get(i);
        }
        // strArrayList is a collection of Strings as you defined.
        ArrayList<Integer> ListOfn = new ArrayList<Integer>(Arrays.asList(arr));
        System.out.println();
        // System.out.println("First Choice of 10 numbers: ---------> " + part);
        Collections.sort(ListOfn);
        return ListOfn;
    }

    /**
     * Will return the collection of cash5 result object @return; a ArrayList of c5
     * result object
     */
    public ArrayList<Cash5Obj> getCash5LineObjectList() {
        return cash5LineObjectList;
    }

    /**
     * Will return a TreeMap collection (key and ordered values map) of cash5 Result
     * 
     * @return
     */
    private TreeMap<Integer, String> getPlayTreeMaps() {
        return playTreeMaps;
    }

    public ArrayList<String[]> getResultDataList() {
        return resultDataList;
    }

    /**
     * This function represents myPoolNumbersToPlayThisGame
     * 
     * @return
     */
    public int[] getPoolNumsToPlayNextGame() {
        return poolNumbersToPlayNextGame;
    }

    public TreeSet<Integer> getSliderElements() {
        return nextSliderElements;
    }

    public void setPoolNumsToPlayNextGame(int[] poolNumbersToPlayNextGame) {
        this.poolNumbersToPlayNextGame = poolNumbersToPlayNextGame;
    }

    public void setPoolNumsToPlayLastGame(int[] poolNumpToPlayLastGame) {
        this.poolNumsToPlayLastGame = poolNumpToPlayLastGame;
    }

    public int[] getPoolNumsToPlayLastGame() {
        return poolNumsToPlayLastGame;
    }

    public String displayTreeMapList() {
        String str = "";
        // using for-each loop for
        // iteration over TreeMap.entrySet()
        for ( Entry<Integer, String> entry : playTreeMaps.entrySet() ) {
            str += entry.toString() + "\n";
        }
        return str;
    }

    /**
     * This function will display information about a cash5 object list
     */
    public String displayCash5ResultsList() {
        String str = "";
        for ( Cash5Obj item : cash5LineObjectList ) {
            str += item.toString();
        }
        return str;
    }

    /**
     * Downloads a file from a specific URL and saves it to disc
     * 
     * @param fileURL HTTP URL of the file to be downloaded
     * @param saveDir path of the directory to save the file
     * @throws IOException
     */
    public static void downloadFile(String fileURL, String saveDir) throws IOException {
        final int BUFFER_SIZE = 4096;
        String gameName = "Cash5_";
        URL url = new URL(fileURL);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        int responseCode = httpConn.getResponseCode();

        // cleanDirectory(saveDir);
        // new File(CommonClass.getMatchingFiles(new File("DowloadedFiles/"),
        // "Cash5").get(0).toString()).delete();

        // always check HTTP response code first
        if ( responseCode == HttpURLConnection.HTTP_OK ) {
            String fileName = gameName + CommonClass.currentDate() + ".txt";

            // opens input stream from the HTTP connection
            InputStream inputStream = httpConn.getInputStream();
            String saveFilePath = saveDir + File.separator + fileName;

            // opens an output stream to save into file
            FileOutputStream outputStream = new FileOutputStream(saveFilePath);

            int bytesRead = -1;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            outputStream.close();
            inputStream.close();

            System.out.println("File Downloaded!");
        }
        else {
            System.out.println("No file to download. Server replied HTTP code: " + responseCode);
        }
        httpConn.disconnect();
    }

    /**
     * @param saveDir
     */
    private static void cleanDirectory(String saveDir) {
        File file = new File(saveDir);
        deleteDirectory(file);
    }

    // function to delete subdirectories and files
    private static void deleteDirectory(File file) {
        // store all the paths of files and folders present
        // inside directory
        for ( File subfile : file.listFiles() ) {

            // if it is a subfolder,e.g Rohan and Ritik,
            // recursiley call function to empty subfolder
            if ( subfile.isDirectory() ) {
                deleteDirectory(subfile);
            }

            // delete files and empty subfolders
            subfile.delete();
        }
    }

    /**
     * This method will dowload the current data file (today's data file) if not
     * present in the Dir. If an old file is present it will be deleted and replaced
     * by today's data file.
     * 
     * @return
     */
    public String manedgeDataFiles() {
        List<File> currentDataFileList = new ArrayList<>();
        // Is there a Cash5 Data File in the dir?
        // getting the list of all files in the dir
        String gameName = "Cash5";
        List<File> matchingFilesList = CommonClass.getMatchingFiles(new File("DowloadedFiles/"), gameName);

        if ( matchingFilesList.isEmpty() ) { // Empty list
            // -if no matching data file at all,then dowload a current file
            try {
                Cash5.downloadFile("https://www.valottery.com/sitecore/shell/api/sitecore/DrawPastNumbers/DownloadAll?gameId=1030", "DowloadedFiles");
            }
            catch (IOException e) {
            }
            currentDataFileList.add(CommonClass.getMatchingFiles(new File("DowloadedFiles/"), gameName).get(0));

        }
        else {// matchingFilesList is not empty. Check if there is a current dat file in it?
            for ( File dataFilePath : matchingFilesList ) {

                if ( !dataFilePath.toString().endsWith(CommonClass.currentDate() + ".txt") ) {// if today's data file has
                                                                                              // not already been
                                                                                              // downloaded
                    dataFilePath.delete();
                }
                else {
                    currentDataFileList.add(dataFilePath);
                }
            } // thi sill produce a ne list that should on contain 1 file

            if ( currentDataFileList.isEmpty() ) {
                try {
                    Cash5.downloadFile("https://www.valottery.com/sitecore/shell/api/sitecore/DrawPastNumbers/DownloadAll?gameId=1030", "DowloadedFiles");
                }
                catch (IOException e) {
                }
            }

        }
        return currentDataFileList.get(0).toString();
    }

    /**
     * This function will print your winning numbers in red in the last Game winning numbers
     */
    public void printMyWinInRed() {
        int k = 0;
        int size = yourWinningNumbersForLastGame(sliderSize).size();
        String[] strArr1 = new String[size];
        List<String> yourWinningArrList;
        String[] GameResArr;
        yourWinningArrList = new ArrayList<String>(Arrays.asList(arrayListToArr(k, strArr1)));
        GameResArr = lastGameWinNums();

        final int GAME_RES_SIZE = lastGameWinNums().length - 1;
        System.out.print("[");
        for ( int i = 0; i < GameResArr.length; i++ ) {
            if ( yourWinningArrList.contains(GameResArr[i]) ) {
                if ( i != GAME_RES_SIZE ) {
                    System.err.print(GameResArr[i] + ", ");
                }
                else {
                    System.err.print(GameResArr[i]);
                }
            }
            else {
                if ( i != GAME_RES_SIZE ) {
                    System.out.print(GameResArr[i] + ", ");
                }
                else {
                    System.out.print(GameResArr[i]);
                }
            }
        }
        System.out.print("]");
    }


    public void createDeepLearningData(int sliderSize) {
        int listSize = cash5LineObjectList.size();
        int resSize = cash5LineObjectList.get(0).getResult().length;

        allGameKey_PoolNumb_MapList = new ArrayList<>();
        map_Key_PoolNumbToPlay_PairsList = new HashMap<>();

        ArrayList<String> arrList;
        ArrayList<ArrayList<String>> wideArrList;

        ArrayList<String> populateCash41 = new ArrayList<>();
        for (Integer item : populateCash41()) {
            populateCash41.add(String.valueOf(item));
        }

        for (int i = listSize; i > sliderSize; i--) {
            wideArrList = new ArrayList<>();
            ArrayList<String> tempPoolNumbersToPlay = new ArrayList<>();

            // Gathering slider elements
            for (int j = 0; j < sliderSize; j++, i--) {
                arrList = new ArrayList<>(Arrays.asList(cash5LineObjectList.get(i - 1).getResult()));
                wideArrList.add(arrList);
            }
            String key = Arrays.toString(cash5LineObjectList.get(i - 1).getResult());
            ArrayList<String> sliderElements = new ArrayList<>();
            for (ArrayList<String> item : wideArrList) {
                sliderElements.addAll(item);
            }

            // Split string with comma
            StringTokenizer strTokenizedKey = new StringTokenizer(key, ";:][, ");
            String[] keyArr = new String[resSize];
            for (int j = 0; j < keyArr.length; j++) {
                keyArr[j] = (String) strTokenizedKey.nextElement();
            }

            // Checking if game result elements are contained in slider elements list
            boolean isKeyArrContained = true;
            for (String element : keyArr) {
                if (!sliderElements.contains(element)) {
                    isKeyArrContained = false;
                    break;
                }
            }
            if (!isKeyArrContained) {
                tempPoolNumbersToPlay.addAll(populateCash41);
                tempPoolNumbersToPlay.removeAll(sliderElements);

                System.out.println("PoolNumberToPlay: " + tempPoolNumbersToPlay);
                System.err.println("Key: " + Arrays.toString(keyArr));

                map_Key_PoolNumbToPlay_PairsList.put(keyArr, tempPoolNumbersToPlay);
            }

            i = i + sliderSize - 1;
        }

        allGameKey_PoolNumb_MapList.add(map_Key_PoolNumbToPlay_PairsList);
    }


    private void displayMap_Key_PoolNumbToPlay_PairsList() {
        // Iterating HashMap through for loop
        for ( Entry<String[], ArrayList<String>> set : map_Key_PoolNumbToPlay_PairsList.entrySet() ) {
            System.err.println(Arrays.toString(set.getKey()) + " -> " + set.getValue());
        }
    }

    /**
     * This function 
     * @param map_Key_PoolNumbToPlay_PairsList
     * @param n = size of the subPoolNumbersToPlay
     * @param m
     */
    public void winLossTestStats(Map<String[], ArrayList<String>> map_Key_PoolNumbToPlay_PairsList, int n, int m) {
        int winTestCount = 0;
        int lostTestCount = 0;
        int keySize = 5;//map_Key_PoolNumbToPlay_PairsList.size();
        ArrayList<String> poolNumberToPlay = null;
        String[] keyResult = new String[keySize];

        for ( Entry<String[], ArrayList<String>> set : map_Key_PoolNumbToPlay_PairsList.entrySet() ) {
            //System.err.println(Arrays.toString(set.getKey()) + " -> " + set.getValue());
            keyResult = set.getKey();
            //System.err.println("keyResult: " + Arrays.toString(keyResult));
            poolNumberToPlay = new ArrayList<>(set.getValue());

            System.out.println("poolNumberToPlay: " + poolNumberToPlay.size());
            if ( n > poolNumberToPlay.size() ) {
                System.err.println("The subPoolNumbersToPlay, n, should be < " + poolNumberToPlay.size());
                // n = poolNumberToPlay.size();
                break;
            }

            //Use Rand function to take n elements of the poolNumbersToPlay to see if pass the winn test

            int counter = 0;
            while (counter != m) {
                String[] arr = new String[n];
                Collections.shuffle(poolNumberToPlay);

                Stack<Integer> stack = new Stack<>();
                for ( int i = 0; i < n; i++ ) {
                    stack.add(i);
                }
                //                System.err.println("n: " + n);
                //if n is high make sure you do not keep choosing same numbers already chosen
                for ( int i = 0; i < n; i++ ) {
                    Collections.shuffle(stack);
                    //System.out.println(stack.peek());
                    arr[i] = poolNumberToPlay.get(stack.pop());
                }
                // strArrayList is a collection of Strings as you defined.
                ArrayList<String> subPoolNumbToPlay = new ArrayList<>(Arrays.asList(arr));
                //               System.out.println("subPoolNumbToPlay of " + n + ": " + subPoolNumbToPlay);
                // System.out.println("First Choice of 10 numbers: ---------> " + part);
                Collections.sort(subPoolNumbToPlay);

                for ( int k = 0; k < keySize; k++ ) {
                    //System.err.println(keyResult[k]);
                    if ( !subPoolNumbToPlay.contains(keyResult[k]) ) {
                        lostTestCount++;
                        break;
                    }
                    else {
                        if ( k + 1 == keySize ) {
                            winTestCount++;

                        }
                    }
                }
                counter++;
            }

        }
        System.out.println("For a subPoolNumbersToPlay with n = " + n + ": WinTestCount: " + winTestCount + ", " + "LostTestCount: " + lostTestCount);
        //System.out.println("Winning percentage = " + (winTestCount / m) * 100);
    }

    private String[] arrayListToArr(int k, String[] strArr1) {
        for ( Integer item : yourWinningNumbersForLastGame(sliderSize) ) {
            strArr1[k] = String.valueOf(item);
            k++;
        }
        return strArr1;
    }

    public String getLastGamewinningNums() {
        return lastGamewinningNums;
    }

    public void setLastGamewinningNums(String lastGamewinningNums) {
        this.lastGamewinningNums = lastGamewinningNums;
    }

    public TreeSet<Integer> getLastSliderElements() {
        return lastSliderElements;
    }

    public String[] lastGameWinNums() {
        return cash5LineObjectList.get(0).getResult();
    }

    public TreeSet<Integer> getNextSliderElements() {
        return nextSliderElements;
    }

    public Map<String[], ArrayList<String>> getMap_Key_PoolNumbToPlay_PairsList() {
        return map_Key_PoolNumbToPlay_PairsList;
    }

    public ArrayList<Map<String[], ArrayList<String>>> getAllGameKey_PoolNumb_MapList() {
        return allGameKey_PoolNumb_MapList;
    }
}
