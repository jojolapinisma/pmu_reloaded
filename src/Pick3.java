import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Pick3 extends RawPmuData
{

    /**
     * A Collection / list of Pick3 Result objects
     */
    ArrayList<Pick3Obj> pick3ResultsList = new ArrayList<>();

    /**
     * This function will parse each data line into a data object and populate the
     * pick3ResultsList collection.
     */
    public void parsePick3RawData() {
        try {
            rawData.forEach(oneLineData -> {
                StringTokenizer stk = new StringTokenizer(oneLineData, ";: ");
                Pick3Obj resultObj = new Pick3Obj();

                if ( stk.countTokens() != 0 ) {
                    if ( stk.countTokens() == 3 ) {
                        resultObj.setDate(stk.nextToken());
                        resultObj.setTime(stk.nextToken());
                        resultObj.setResult(resultObj.parseToArray(stk.nextToken()));

                        pick3ResultsList.add(resultObj);
                    }
                    else if ( stk.countTokens() == 5 ) {
                        String date = stk.nextToken();
                        for ( int i = 0; i < 2; i++ ) {
                            resultObj.setDate(date);
                            resultObj.setTime(stk.nextToken());
                            resultObj.setResult(resultObj.parseToArray(stk.nextToken()));
                            pick3ResultsList.add(resultObj);

                            resultObj = new Pick3Obj();
                        }

                        resultObj = new Pick3Obj();
                    }
                    else if ( stk.countTokens() == 9 ) {
                        String date = stk.nextToken();
                        for ( int i = 0; i < 2; i++ ) {
                            resultObj.setDate(date);
                            resultObj.setTime(stk.nextToken());
                            resultObj.setResult(resultObj.parseToArray(stk.nextToken()));
                            stk.nextToken(); // to skip 1 token
                            resultObj.setFireball(stk.nextToken());
                            pick3ResultsList.add(resultObj);

                            resultObj = new Pick3Obj();
                        }
                    }
                }

            });
        }
        catch (Exception e) {
            System.err.println("Error in the file containt");
            System.out.println("Check data in the file");
            System.exit(0);
        }
        // System.out.println(pick3Results);
    }

    /**
     * Will return the collection of Pick3 result object @return; a ArrayList of c5
     * result object
     */
    public ArrayList<Pick3Obj> getPick3ResultsList() {
        return pick3ResultsList;
    }

    public void displayPick3ParsedData() {
        System.out.print("\nPICK 4 PAST RESULTS\n");
        System.out.print("===================\n");
        for ( Pick3Obj obj : pick3ResultsList ) {
            pick3ObjDisplay(obj);
        }

    }

    /**
     * This function will display information about a Pick3 result object
     * 
     * @param obj: One line result data
     */
    private void pick3ObjDisplay(Pick3Obj obj) {
        System.out.print("Date: " + obj.getDate());
        System.out.print("\nResults: " + Arrays.toString(obj.getResult()));
        if ( obj.getTime() == null ) {
            System.out.print("\nTime of Day: Once a Day." + "\n\n");
        }
        else {
            System.out.print("\nTime of Day: " + obj.getTime() + "\n");
        }
        System.out.print("Fireball: " + obj.getFireball() + "\n\n");
    }

}
