import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class CommonClass
{
    public static enum GameName {
        CASH5, BANK_A_MILLION, CASH_4_LIFE, MEGAMILLIONS, POWERBALL, CASH3, CASH4;
    }

    private static String gameDataPath;
    private static String gameKind;

    // Prints a line about Day using switch
    public static String gameDataPath(GameName gameName) {
        //String gameDataPath = "";

        switch ( gameName ) {
        case CASH5:
            gameKind = "Cash5";
            gameDataPath = "https://www.valottery.com/api/v1/downloadall?gameId=1030";
            break;
        case BANK_A_MILLION:
            gameKind = "BankaMillion";
            gameDataPath = "https://www.valottery.com/api/v1/downloadall?gameId=1070";
            break;
        case CASH_4_LIFE:
            gameKind = "Cash4Life";
            gameDataPath = "https://www.valottery.com/api/v1/downloadall?gameId=1065";
            break;
        case MEGAMILLIONS:
            gameKind = "MegaMillion";
            gameDataPath = "https://www.valottery.com/api/v1/downloadall?gameId=15";
            break;
        case POWERBALL:
            gameKind = "PowerBall";
            gameDataPath = "https://www.valottery.com/api/v1/downloadall?gameId=20";
            break;
        case CASH3:
            gameKind = "Cash3";
            //gameDataPath = 
            break;
        case CASH4:
            gameKind = "Cash4";
            //gameDataPath = 
            break;
        default:
            gameKind = "Cash5";
            System.out.println("No Game muched.");
            break;
        }
        return gameDataPath;
    }

    public static List<File> getMatchingFiles(File parent, final String extension) {
        File[] files = parent.listFiles(new FileFilter() {

            public boolean accept(File dir) {
                String name = dir.getName();
                if ( name.contains(extension) ) {
                    return true;
                }
                return false;
            }
        });

        List<File> retval = Arrays.asList(files);
        return retval;
    }

    /**
     * This method will return the current date in the specified string format
     * 
     * @return
     */
    public static String currentDate() {
        String pattern = "MM-dd-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        return date;
    }

    /**
     * Downloads a file from a specific URL and saves it to disc
     * 
     * @param fileURL HTTP URL of the file to be downloaded
     * @param saveDir path of the directory to save the file
     * @throws IOException
     */
    public static void downloadFile(String fileURL, String saveDir) throws IOException {
        final int BUFFER_SIZE = 4096;
        String gameName = gameKind + "_";
        URL url = new URL(fileURL);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        int responseCode = httpConn.getResponseCode();

        // always check HTTP response code first
        if ( responseCode == HttpURLConnection.HTTP_OK ) {
            String fileName = gameName + CommonClass.currentDate() + ".txt";

            // opens input stream from the HTTP connection
            InputStream inputStream = httpConn.getInputStream();
            String saveFilePath = saveDir + File.separator + fileName;

            // opens an output stream to save into file
            FileOutputStream outputStream = new FileOutputStream(saveFilePath);

            int bytesRead = -1;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            outputStream.close();
            inputStream.close();

            System.out.println("File Downloaded!");
        }
        else {
            System.out.println("No file to download. Server replied HTTP code: " + responseCode);
        }
        httpConn.disconnect();
    }

    /**
     * This method will dowload the current data file (today's data file) if not
     * present in the Dir. If an old file is present it will be deleted and replaced
     * by today's data file.
     * @param gameName 
     * @param c5 
     * 
     * @return
     */
    static String manedgeDataFiles(GameName gameName) {
        gameDataPath(gameName);

        List<File> currentDataFileList = new ArrayList<>();
        // Is there a Cash5 Data File in the dir?
        // getting the list of all files in the dir
        //String gameName = "Cash5";
        List<File> matchingFilesList = getMatchingFiles(new File("DowloadedFiles/"), gameKind);

        if ( matchingFilesList.isEmpty() ) { // Empty list
            // -if no matching data file at all,then dowload a current file
            try {
                downloadFile(gameDataPath, "DowloadedFiles");
            }
            catch (IOException e) {
            }
            currentDataFileList.add(getMatchingFiles(new File("DowloadedFiles/"), gameKind).get(0));

        }
        else {// matchingFilesList is not empty. Check if there is a current dat file in it?
            for ( File dataFilePath : matchingFilesList ) {

                if ( !dataFilePath.toString().endsWith(CommonClass.currentDate() + ".txt") ) {// if today's data file has
                                                                                              // not already been
                                                                                              // dowloaded
                    dataFilePath.delete();
                }
                else {
                    currentDataFileList.add(dataFilePath);
                }
            } // thi sill produce a ne list that should on contain 1 file

            if ( currentDataFileList.isEmpty() ) {
                try {
                    downloadFile(gameDataPath, "DowloadedFiles");
                    currentDataFileList.add(getMatchingFiles(new File("DowloadedFiles/"), gameKind).get(0));
                }
                catch (IOException e) {
                }
            }

        }
        return currentDataFileList.get(0).toString();
    }

}
