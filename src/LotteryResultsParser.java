import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

public class LotteryResultsParser {
    static class CustomKey implements Comparable<CustomKey> {
        List<Integer> key;

        public CustomKey(List<Integer> key) {
            this.key = key;
        }

        @Override
        public int compareTo(CustomKey other) {
            return this.key.toString().compareTo(other.key.toString());
        }
    }

    public static void make_DeepLearningData(Path rawPmuDataPath, CommonClass.GameName gameName, int slyderSize) {
        String filename = rawPmuDataPath.toString();
        Map<Date, String> lotteryResults = null;
        if (gameName.equals(CommonClass.GameName.CASH5)) {
            lotteryResults = parseCash5_LotteryNumbers(filename);
            printLottoDatesResults(lotteryResults);
        } else {
            lotteryResults = parseLotteryResults(filename);
            printLottoDatesResults(lotteryResults);
        }

        List<String> lotteryNumberslist = convertMapToList(lotteryResults);
        printLotValues(lotteryNumberslist);// no order

        printLearningData(make_LottoPool_WinNumbres(lotteryNumberslist, slyderSize), gameName, slyderSize);
        System.out.println("The End!! ");
    }

    public static void printLearningData(Map<LotteryResultsParser.CustomKey, List<Integer>> learningData,
            CommonClass.GameName gameName, int slyderSize) {
        System.out.println("\n\nCREATE " + gameName + " DEEPLEARNING DATA--> <PlayPool>, <Winner Mumbers>\n"
                + "These are the winning numbers presents in the playpool of slider: " + slyderSize + ".");
        for (Map.Entry<LotteryResultsParser.CustomKey, List<Integer>> entry : learningData.entrySet()) {

            if (entry.getValue().containsAll(entry.getKey().key)) {
                System.out.println("Play Pool: " + entry.getValue());
                System.out.println("Winner Numbers: " + entry.getKey().key);
            }

        }
    }

    /**
     * Building Key values pairs: Keys=lotto pool, values = winning numbers
     * 
     * @param lotteryNumbers
     * @param SLIDER_SIZE
     * @return
     */
    public static Map<LotteryResultsParser.CustomKey, List<Integer>> make_LottoPool_WinNumbres(
            List<String> lotteryNumbers, int SLIDER_SIZE) {
        Map<LotteryResultsParser.CustomKey, List<Integer>> learningData = new TreeMap<>();

        List<List<Integer>> parsedNumbers = new ArrayList<>();
        for (String str : lotteryNumbers) {
            List<Integer> numbers = new ArrayList<>();
            String[] parts = str.split(",");
            for (String part : parts) {
                numbers.add(Integer.parseInt(part.trim()));
            }
            parsedNumbers.add(numbers);
        }

        for (int a = 0, b = SLIDER_SIZE; b < parsedNumbers.size(); a++, b++) {
            List<Integer> playPool = new ArrayList<>();
            List<List<Integer>> sliderLists = new ArrayList<>();
            TreeSet<Integer> sliderElements = new TreeSet<>();
            List<Integer> gamePool = new ArrayList<>();

            for (int i = 1; i <= 69; i++) {
                gamePool.add(i);
            }

            for (int i = a + 1; i < b + 1; i++) {
                sliderLists.add(parsedNumbers.get(i));
            }

            for (List<Integer> list : sliderLists) {
                sliderElements.addAll(list);
            }

            playPool.addAll(gamePool);
            playPool.removeAll(sliderElements);

            List<Integer> winnerNumber = parsedNumbers.get(a);

            learningData.put(new LotteryResultsParser.CustomKey(winnerNumber), playPool);

            sliderElements.clear();
        }
        return learningData;
    }

    /**
     * Parses a lottery results file and creates a mapping of dates to lottery
     * numbers. The input file should have each line in the format "date; number1,
     * number2, number3, ...".
     *
     * @param filename The name of the file to read lottery results from.
     * @return A mapping of dates to lottery numbers in the format Map<Date,
     *         String>. The keys are dates representing the lottery draw dates, and
     *         the values are strings containing the comma-separated lottery numbers
     *         for that draw.
     * @throws IOException If there is an error reading the file.
     */
    public static Map<Date, String> parseLotteryResults(String filename) {
        // Use a custom comparator to sort the dates in reverse order (descending)
        Map<Date, String> lotteryResults = new TreeMap<>(Collections.reverseOrder());

        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] parts = line.split("; ");
                if (parts.length == 3) {
                    String dateString = parts[0];
                    String numbers = parts[1] + "," + parts[2].split(": ")[1];
                    Date parsedDate = parseDate(dateString);
                    lotteryResults.put(parsedDate, numbers);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return lotteryResults;
    }

    public static Date parseDate(String dateString) {
        SimpleDateFormat format = new SimpleDateFormat("M/d/yyyy");
        try {
            return format.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<String> convertMapToList(Map<Date, String> map) {
        List<String> valuesList = new ArrayList<>();

        // leaving the dates behind. continuing with the values
        for (String value : map.values()) {
            valuesList.add(value);
        }

        return valuesList;
    }

    public static List<String> convertMapToList_withOrderKept(Map<Date, String> map) {
        List<String> valuesList = new ArrayList<>();

        // Using LinkedHashMap to maintain insertion order of values
        Map<Date, String> linkedMap = new LinkedHashMap<>(map);

        // Leaving the dates behind, continuing with the values
        for (String value : linkedMap.values()) {
            valuesList.add(value);
        }

        return valuesList;
    }

    private static void printLotValues(List<String> valuesList) {
        System.out.println(" ");
        for (String value : valuesList) {
            System.out.println("Valuek: " + value);
        }
        System.out.println(" ");
    }

    private static void printLottoDatesResults(Map<Date, String> lotteryResults) {
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT);
        System.out.println(" ");
        for (Map.Entry<Date, String> entry : ((TreeMap<Date, String>) lotteryResults).entrySet()) {
            Date date = entry.getKey();
            String numbers = entry.getValue();
            String formattedDate = dateFormat.format(date);
            System.out.println("Date: " + formattedDate + ", Numbers: " + numbers);
            lotteryResults.put(date, numbers);
        }
        System.out.println(" ");
    }

    // Parse the lottery numbers and print the results
    private static Map<Date, String> parseCash5_LotteryNumbers(String fileName) {

        List<String> lotteryNumbers = null;
        try {
            lotteryNumbers = readLotteryNumbersFromFile(fileName);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Map<Date, String> lotteryResults = new TreeMap<>(Collections.reverseOrder());
        Map<LotteryResultsParser.CustomKey, List<Integer>> learningData = new TreeMap<>();

        for (String line : lotteryNumbers) {
            if (line.contains("Day:") && line.contains("Night")) {
                String[] parts = line.split(";");
                if (parts.length == 3) {
                    String dateString = parts[0].trim();
                    String dayPlay = parts[1].trim().split(":")[1].trim();
                    String nightPlay = parts[2].trim().split(":")[1].trim();
                    Date parsedDate = parseDate(dateString);
                    lotteryResults.put(parsedDate, dayPlay);
                    lotteryResults.put(parsedDate, nightPlay);
                }
            } else if (!line.contains("Day:") && line.contains("Night")) {
                String[] parts = line.split("; Night: ");
                if (parts.length == 2) {
                    String dateString = parts[0].trim();
                    String numbersString = (parts[1]).trim();
                    Date parsedDate = parseDate(dateString);
                    lotteryResults.put(parsedDate, numbersString);
                }

            } else {
                String[] parts = line.split(";");
                if (parts.length == 2) {
                    String dateString = parts[0].trim();
                    String numbersString = parts[1].trim();
                    Date parsedDate = parseDate(dateString);
                    lotteryResults.put(parsedDate, numbersString);
                }
            }

        }
        return lotteryResults;
    }

    // Read the lottery numbers from the file and return them as a list
    private static List<String> readLotteryNumbersFromFile(String fileName) throws IOException {
        List<String> lotteryNumbers = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = reader.readLine()) != null) {
                lotteryNumbers.add(line);
            }
        }

        return lotteryNumbers;
    }

}
