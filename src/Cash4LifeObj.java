import java.util.StringTokenizer;

public class Cash4LifeObj
{

    String   date;
    String[] result;
    String   cashBall;

    /**
     * This function will turn the String result into an Array result
     * 
     * @param result PMU String result
     * @return PMU Array result
     */

    public String[] parseToArray(String result) {
        StringTokenizer stk = new StringTokenizer(result, ",");
        String[] resArr = new String[stk.countTokens()];
        int lenght = stk.countTokens();

        for ( int i = 0; i < lenght; i++ ) {
            resArr[i] = stk.nextToken();
        }
        return resArr;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String[] getResult() {
        return result;
    }

    public void setResult(String[] result) {
        this.result = result;
    }

    public String getCashBall() {
        return cashBall;
    }

    public void setCashBall(String cashBall) {
        this.cashBall = cashBall;
    }

    @Override
    public String toString() {
        return "ResultObj [date=" + date + ", result=" + result + ", cashBall=" + "]";
    }

}
