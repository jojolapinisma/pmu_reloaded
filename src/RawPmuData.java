import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class RawPmuData
{

    protected List<String> rawData;

    protected void initializeRawData(Path path) {
        readRawPmuDataFile(path);
    }

    /**
     * This function will filter out unwanted lines from the raw data, convert all
     * content to upper case and load the useful data into memory as a List. -JJL
     */
    public void readRawPmuDataFile(Path path) {
        try (Stream<String> stream = Files.lines(Paths.get(path.toString()))) {
            rawData = stream.filter(line -> !line.startsWith("Results ")).filter(line -> !line.startsWith("All")).filter(line -> !line.startsWith("There")).map(String::toUpperCase)
                    .collect(Collectors.toList());
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }// End of readRawPmuDataFile

    /**
     * This function will display raw data from memory If memory is empty, data have
     * not been read yet, data will be read using the raw data file path, then
     * displayed.
     * 
     * @param path -> raw data file path
     */
    public void displayRawData() {
        rawData.forEach(System.out::println);
    }

    /**
     * This function will pull data from the Internet using the URL provided
     * @param url
     * @throws IOException
     */
    public void netPull(URL url) {
        try {
            InputStream content = url.openStream();
            int c;
            while ((c = content.read()) != -1)
                System.out.print((char) c);
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
}
